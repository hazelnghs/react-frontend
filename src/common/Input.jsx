import React from 'react';


const Input = ({ name, label, value, type, onChange, error, placeholder, isOptional }) => {

    return (
        <div className="form-group"> 
            <label htmlFor={name}>{label}
                {isOptional === 'true' ?  <span className="text-muted"> (Optional)</span> : ''}
            </label>
            <input  type={type} 
                    value={value}
                    name={name}
                    onChange={onChange}
                    placeholder={placeholder}
                    className="form-control" id={name} />

            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
};

export default Input;