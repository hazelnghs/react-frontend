import React, { Component } from 'react';

class ListGroup extends Component {
    
    render() {
        const {categories, onClick, selectedCategory} = this.props;

        return (
            <ul className="list-group sticky-top pointer-icon" style={{ 'top': '80px' }}>
            {
                categories.map(category => (
                   
                    <li className={ category === selectedCategory ? "list-group-item active" : "list-group-item" }
                        key={category.id}
                        onClick={() => onClick(category)}> 
                         {category.name}
                    </li>
                ))
            }
            </ul>
        );
    }
}

export default ListGroup;
