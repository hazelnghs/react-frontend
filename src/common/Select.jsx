import React from 'react';

const Select = ({label, name, error, selections, selectedItem, onSelectChange}) => {

    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <select className="custom-select d-block w-100 form-control"
                    id={name} name={name}
                    value={selectedItem}
                    onChange={onSelectChange}>
                {
                    selections.map( (item, index) => (
                        <option key={index} value={item}>
                            {item}
                        </option>
                    ))
                }
            </select>

            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
};

export default Select;