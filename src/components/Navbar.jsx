import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';

class Navbar extends Component {

    toggleNavbar = () => {

        let el = document.getElementById('responsiveNavbar').classList.toggle('collapse');
        // if (el.contains("collapse")) {
        //     el.remove("collapse");
        // } else {
        //     el.add("collapse");
        // }
    };

    handleNav = () => {
        // console.log('selected');
        // let navbar = document.getElementById("responsiveNavbar");
        // navbar.className += " collapse";

        document.getElementById('responsiveNavbar').classList.toggle('collapse');
    };



    toggleDropdown = () => {
        document.getElementById("dropDownContent").classList.toggle("show");

    };

    handleDropdown = () => {
        document.getElementById("dropDownContent").classList.toggle("show");
    };

    render() {

        return (
            <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">

                <NavLink className="navbar-brand" to="/">
                    <span className='fa fa-2x fa-home'> </span>
                </NavLink>


                <button className="navbar-toggler" type="button"
                        onClick={this.toggleNavbar}
                        data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <NavLink className="cart" to="/reviewOrder">
                    <label className="badge badge-primary">{this.props.totalItems}</label>&nbsp;&nbsp;
                    <span className="far fa-shopping-cart"></span>
                </NavLink>

                <div className="collapse navbar-collapse" id="responsiveNavbar" onClick={this.handleNav}>

                    <div className="navbar-nav mr-auto">

                        <NavLink className="nav-item nav-link" to="/menu">Menu</NavLink>

                        <NavLink className="nav-item nav-link" to="/reviewOrder">Review Order</NavLink>

                        {/*<NavLink className="nav-item nav-link" to="/manageMenu">Manage Menu</NavLink>*/}

                        {/*<NavLink className="nav-item nav-link" to="/manageOrder">Manage Order</NavLink>*/}

                        <div className="dropdown show">
                            <a className="nav-item nav-link dropdown-toggle pointer-icon"
                               onClick={this.toggleDropdown}
                               id="dropdownMenuLink">
                                Management
                            </a>
                            <div className="dropdown-menu"
                                 id="dropDownContent"
                                 onClick={this.handleDropdown}
                                 style={{background: '#ddd'}}>
                                <NavLink className="dropdown-item nav-item nav-link"
                                         style={{color: 'black'}}
                                         to="/manageMenu">Manage Menu
                                </NavLink>

                                <NavLink className="dropdown-item nav-item nav-link"
                                         style={{color: 'black'}}
                                         to="/manageOrder">Manage Order
                                </NavLink>
                            </div>
                        </div>
                
                    </div>
                        
                    {/*<div className="navbar-nav ml-auto">*/}

                        {/*<NavLink className="nav-item nav-link" to="/register">Register</NavLink>*/}

                        {/*<NavLink className="nav-item nav-link" to="/login">Login</NavLink>   */}

                    {/*</div>*/}
                </div>



                {/*<NavLink className="navbar-brand" to="/">*/}
                {/*    <span className='fa fa-2x fa-home'> </span>*/}
                {/*</NavLink>*/}
            </nav>
        );
    }
}

export default Navbar;
