import React from 'react';
import Form from '../common/Form';
import Joi from 'joi-browser';

class Register extends Form {

    state = {
        data: {
            username: '',
            password: ''
        },
        errors: {}
    }

    dataSchema = {
        username: Joi.string().min(10).required(),
        password: Joi.string().min(10).required()
    }

    render() {
        return (
            <div>
                <h1>Register Form</h1>
                <form>
                    {this.renderInput("username", "Username", "text")}
                    {this.renderInput("password", "Password", "password")}
                </form>

                {this.renderButton('Register')}
            </div>
        );
    }
}

export default Register;