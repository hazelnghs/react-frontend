import React from 'react';

const CheckoutCart = ({totalItems, itemsInCart, totalPrice }) => {
    return (
        <React.Fragment>
            <h4 className="d-flex justify-content-between align-items-center mb-3">
                <span className="text-muted">Your orders</span>
                <span className="badge badge-secondary badge-pill">{totalItems}</span>
            </h4>
            <ul className="list-group mb-3">

                {
                    itemsInCart.map(item => (
                        <li className="list-group-item d-flex justify-content-between lh-condensed" key={item._id}>
                            <div>
                                <h6 className="my-0">{item.name}</h6>
                                <small className="text-muted">x {item.quantity}</small>
                            </div>
                            <span className="text-muted">${item.price * item.quantity}</span>
                        </li>
                    ))
                }
                <li className="list-group-item d-flex justify-content-between">
                    <span>Total (SGD)</span>
                    <strong>${totalPrice}</strong>
                </li>
            </ul>
        </React.Fragment>
    );
};

export default CheckoutCart;

