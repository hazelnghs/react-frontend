import React, {Component} from 'react';
import orderService from "../../service/orderService";
import TableHeader from "../../common/TableHeader";
import TableLengthFilterWrapper from "../../common/TableLengthFilterWrapper";

class OrderPlaced extends Component {

    state = {
        orderDetails: []
    };

    async componentDidMount() {
        const {data} = await orderService.getOrders();
        console.log({data});

        this.setState({orderDetails: data});
    }

    render() {

        const {location} = this.props;
        const {orderDetails} = this.state;

        return (
            <div className="col-lg-10 col-md-10">

                <div className="card">
                    <TableHeader
                        currentItems={orderDetails.length}
                        currentPath={location.pathname}/>

                    <div className="card-body">
                        {/*<TableLengthFilterWrapper*/}
                        {/*    // searchInput={searchInput}*/}
                        {/*    // selectedLength={selectedLength}*/}
                        {/*    // pageLengthArr={pageLengthArr}*/}
                        {/*    onSelectPageLength={this.handleSelectPageLength}*/}
                        {/*    onInputChange={this.onSearchInputChange}*/}
                        {/*    onSearchInput={this.handleSearchMenu}/>*/}

                        <table className="table-responsive-md table table-bordered table-striped col" >
                            <thead>
                            <tr>
                                <th width="20%">Order Date</th>
                                <th width="20%">Name</th>
                                <th width="20%">Email</th>
                                <th width="20%">Total Price</th>
                                <th width="20%">Address</th>
                            </tr>
                            </thead>

                            <tbody>
                            {this.renderOrders()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

    convertDate(oriDate) {
        console.log('iso date ', oriDate);
        console.log('js date ', new Date(oriDate));
        return new Date(oriDate).toLocaleString();
    }


    renderOrders() {
        return (
            this.state.orderDetails.map(order => (
                <tr key={order._id}>
                    <td>{this.convertDate(order.datePlaced)}</td>
                    <td>{order.shipping.firstName} {order.shipping.lastName}</td>
                    <td>{order.shipping.email}</td>
                    <td>${order.items.totalPrice}</td>
                    <td>
                        {order.shipping.address.address1}, {order.shipping.address.address2}
                    </td>
                </tr>
            ))
        );
    }
}

export default OrderPlaced;
