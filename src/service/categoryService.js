import http from './httpService';

const apiEndpoint = "/category";


export async function getMenuCategories() {
    return await http.get(apiEndpoint);
};

export default {
    getMenuCategories
}