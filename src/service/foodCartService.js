import http from './httpService';

const apiEndpoint = "/carts";



export async function addToCart(menu) {
    const cartId = await getOrCreateCartId();
    return await updateCartMenu(cartId, menu);
}


export async function updateOrder(cartId, menu) {
    return await updateFoodCartItems(cartId, menu);
}


export async function getItemsInCart() {

    const cartId = localStorage.getItem('cart-id');
    if(cartId) {
        try {
            const result = await http.get(apiEndpoint + "/" + cartId);
            // console.log("find cart id result ", result);
            return result;

        } catch (e) {
            console.error(e);
            localStorage.removeItem('cart-id');
            return null;
        }
    }

}


export async function getCartTotalItem() {
    const cartId = await getOrCreateCartId();
    const result = await getItemsInCart();

    const totalItems = this.calcTotalItemsInCart(result);
    // console.log("total items in cart " , totalItems);
    return totalItems;
}


async function getOrCreateCartId() {
    const cartId = localStorage.getItem('cart-id');
    if(cartId) return cartId;

    const newCart = await createCart();
    localStorage.setItem('cart-id', newCart.data._id);
    return newCart.data._id;
}

async function createCart() {
    return await http.post(apiEndpoint);
}


async function updateCartMenu(cartId, menu) {
    console.log("update menu in cart");

    // 1. get existing cart
    // 2. check items if exist
    //     -increase quantity if exist
    //     -add new item if not exist

    const existingCart = await getItemsInCart();
    const cart = existingCart.data;

    if(cart.items.length > 0) {
        const foundItem = cart.items.filter(item => item._id === menu._id);
        if(foundItem.length > 0) {
            foundItem[0].quantity += 1;
        }
        else {
            cart.items.push(mapNewMenu(menu));
        }
    } else {
        cart.items.push(mapNewMenu(menu));
    }

    console.log(cart.items);
    return await updateFoodCartItems(cartId, cart.items);
}

async function updateFoodCartItems(cartId, items) {
    return await http.post(apiEndpoint + "/" + cartId, items);
}


export function calcTotalItemsInCart(result) {
    let totalItems = 0;
    result.data.items.forEach(item => {
        totalItems = totalItems += item.quantity;
    });

    return totalItems;
}


function mapNewMenu(menu) {

    let item = {
        '_id': menu._id,
        'name': menu.name,
        'price': menu.price,
        'imageUrl': menu.imageUrl,
        'quantity': 1
    };
    return item;
}



export default {
    addToCart,
    getItemsInCart,
    updateOrder,
    getCartTotalItem,
    calcTotalItemsInCart
}
